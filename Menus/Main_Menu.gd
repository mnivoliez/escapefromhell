extends Control

var start_menu
var options_menu

export (String, FILE) var limbo_scene

func _ready():
    start_menu = $Start_Menu
    options_menu = $Options_Menu
    
    $Start_Menu/Button_Start.connect("pressed", self, "start_menu_button_pressed", ["start"])
    $Start_Menu/Button_Open_Repo.connect("pressed", self, "start_menu_button_pressed", ["open_repo"])
    $Start_Menu/Button_Options.connect("pressed", self, "start_menu_button_pressed", ["options"])
    $Start_Menu/Button_Quit.connect("pressed", self, "start_menu_button_pressed", ["quit"])
    
    $Options_Menu/Button_Back.connect("pressed", self, "options_menu_button_pressed", ["back"])
    $Options_Menu/Button_Fullscreen.connect("pressed", self, "options_menu_button_pressed", ["fullscreen"])
    $Options_Menu/Check_Button_VSync.connect("pressed", self, "options_menu_button_pressed", ["vsync"])
    $Options_Menu/Check_Button_Debug.connect("pressed", self, "options_menu_button_pressed", ["debug"])
        
    Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
    
func start_menu_button_pressed(button_name):
    if button_name == "start":
        get_node("/root/Globals").load_new_scene(limbo_scene)    
    elif button_name == "open_repo":
        OS.shell_open("https://gitlab.deep-nope.me/mnivoliez/escapefromhell")
    elif button_name == "options":
        options_menu.visible = true
        start_menu.visible = false
    elif button_name == "quit":
        get_tree().quit()
        
func options_menu_button_pressed(button_name):
    if button_name == "back":
        start_menu.visible = true
        options_menu.visible = false
    elif button_name == "fullscreen":
        OS.window_fullscreen = !OS.window_fullscreen
    elif button_name == "vsync":
        OS.vsync_enabled = $Options_Menu/Check_Button_VSync.pressed
    elif button_name == "debug":
        get_node("/root/Globals").set_debug_display($Options_Menu/Check_Button_Debug.pressed)
