extends MeshInstance

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal player_in_portal

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.

func _on_Area_body_entered(body):
    if body.name == "Player":
        print_debug("player in portal")
        emit_signal("player_in_portal")

