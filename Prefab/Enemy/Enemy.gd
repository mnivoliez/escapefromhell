extends KinematicBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal seen

var FOV = 45;
var FAR_SIGHT = 30;

var player;
var player_in_range = false

var VERIFICATION_TIMING = 0.5
var elapse = 0

# Called when the node enters the scene tree for the first time.
func _ready():
    var shape = $Sight/CollisionShape.shape
    shape.set_radius(2 * FAR_SIGHT)
    var spotlight = $Rotation_Helper/Head/Face/SpotLight
    spotlight.spot_angle = FOV
    spotlight.spot_range = FAR_SIGHT

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    pass
        
    
func _physics_process(delta):
    elapse += delta
    if elapse < VERIFICATION_TIMING:
        return
    else:
        elapse = 0
    if player_in_range:
        var space_state = get_world().direct_space_state
        var enemy_pos = global_transform.origin
        var player_pos = player.global_transform.origin + Vector3(0,1,0)
        var player_dir = player_pos - enemy_pos
        var sigh_dir = $Rotation_Helper/Head.global_transform.basis.z
        var norm_player_dir = player_dir.normalized()
        var norm_sight_dir = sigh_dir.normalized()
        var dot_sight_player = norm_player_dir.dot(norm_sight_dir)
        var cos_fov = cos(deg2rad(FOV))
        if dot_sight_player > 0 and dot_sight_player > cos_fov:
            var hit = space_state.intersect_ray(enemy_pos, player_pos)
            if hit and hit.collider == player:
                emit_signal("seen")


func _on_Area_body_entered(body):
    if body.name == "Player":
        print_debug("Player entered sight area.")
        player = body
        player_in_range = true

func _on_Area_body_exited(body):
    if body.name == "Player":
        print_debug("Player exited sight area.")
        player = null
        player_in_range = false
