extends KinematicBody

const GRAVITY = -24.8
var vel = Vector3()
const MAX_SPEED = 20
const JUMP_SPEED = 18
const ACCEL = 4.5

var dir = Vector3()

const DEACCEL = 16
const MAX_SLOPE_ANGLE = 40

var camera
var rotation_helper

var MOUSE_SENSITIVITY = 0.05

const MAX_SPRINT_SPEED = 30
const SPRINT_ACCEL = 18
var is_sprinting = false
var CROUCHING_SPEED = 2
var is_crouching = false

var flashlight

var game_ended = false
const END_TIME = 3
var ended_time_elapse = 0
var globals

func _ready():
    camera = $Rotation_Helper/Camera
    rotation_helper = $Rotation_Helper

    Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
    
    flashlight = $Rotation_Helper/Flashlight
    
    globals = get_node("/root/Globals")

func _process(delta):
    if game_ended:
        process_end(delta)

func _physics_process(delta):
    if !game_ended:
        process_input(delta)
        process_movement(delta)

func process_input(delta):

    # ----------------------------------
    # Walking
    dir = Vector3()
    var cam_xform = camera.get_global_transform()

    var input_movement_vector = Vector2()

    if Input.is_action_pressed("movement_forward"):
        input_movement_vector.y += 1
    if Input.is_action_pressed("movement_backward"):
        input_movement_vector.y -= 1
    if Input.is_action_pressed("movement_left"):
        input_movement_vector.x -= 1
    if Input.is_action_pressed("movement_right"):
        input_movement_vector.x += 1

    input_movement_vector = input_movement_vector.normalized()

    dir += -cam_xform.basis.z.normalized() * input_movement_vector.y
    dir += cam_xform.basis.x.normalized() * input_movement_vector.x
    # ----------------------------------

    # ----------------------------------
    # Jumping
    if is_on_floor():
        if Input.is_action_just_pressed("movement_jump"):
            vel.y = JUMP_SPEED
    # ----------------------------------

    # ----------------------------------
    # Capturing/Freeing cursor
    if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
        Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
    # ----------------------------------
    
    # ----------------------------------
    # Sprinting
    if Input.is_action_pressed("movement_sprint"):
        is_sprinting = true
    else:
        is_sprinting = false
    # ----------------------------------
    
    # ----------------------------------
    # Turning the flashlight on/off
    if Input.is_action_just_pressed("flashlight"):
        if flashlight.is_visible_in_tree():
            flashlight.hide()
        else:
            flashlight.show()
    # ----------------------------------
    
    # ----------------------------------
    # Crouch
    is_crouching =  Input.is_action_pressed("movement_crouch")
    # ----------------------------------

func process_movement(delta):
    dir.y = 0
    dir = dir.normalized()
    vel.y += delta * GRAVITY
    
    var hvel = vel
    hvel.y = 0
    
    var target = dir
    if is_sprinting:
        target *= MAX_SPRINT_SPEED
    else:
        target *= MAX_SPEED
    
    var accel
    if dir.dot(hvel) > 0:
        if is_sprinting:
            accel = SPRINT_ACCEL
        else:
            accel = ACCEL
    else:
        accel = DEACCEL        
        
    if is_crouching:
        if self.scale.y > 0.5:
            self.scale.y -= CROUCHING_SPEED * delta
    else:
        if self.scale.y < 1:
            self.scale.y += CROUCHING_SPEED * delta
        
    hvel = hvel.linear_interpolate(target, accel * delta)
    vel.x = hvel.x
    vel.z = hvel.z
    vel = move_and_slide(vel, Vector3(0,1,0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))
    
func process_end(delta):
    if ended_time_elapse < END_TIME:
        ended_time_elapse += delta
    else:
        globals.load_new_scene(globals.MAIN_MENU_PATH)
    
func create_sound(sound_name, position=null):
    globals.play_sound(sound_name, false, position)
    
func on_seen_by_ennemy():
    game_ended = true
    $HUD/End_screen/Label.text = "You died"
    $HUD/End_screen.visible = true
    
func _input(event):
    if game_ended:
        return
    if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
        rotation_helper.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY))
        self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1))
        
        var camera_rot = rotation_helper.rotation_degrees
        camera_rot.x = clamp(camera_rot.x, -70, 70)
        rotation_helper.rotation_degrees = camera_rot


func _on_Portal_player_in_portal():
    print_debug("signal received")
    game_ended = true
    $HUD/End_screen/Label.text = "You won"
    $HUD/End_screen.visible = true
