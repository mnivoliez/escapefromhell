# Escape From Hell

[![pipeline status](https://gitlab.deep-nope.me/mnivoliez/escapefromhell/badges/master/pipeline.svg)](https://gitlab.deep-nope.me/mnivoliez/escapefromhell/commits/master)

[https://mnivoliez.pages.gitlab.deep-nope.me/escapefromhell/](https://mnivoliez.pages.gitlab.deep-nope.me/escapefromhell/)

## Overview

The protagonist (player) is a recently dead person that do not wish to pass the hell trial. So, he tries to escape hell and its pawns.

## Gameplay
The game is a FPS.

The player will have to make its way down to the purgatory (9th circle of Hell according to [Dante's inferno](https://en.wikipedia.org/wiki/Inferno_(Dante))).
The game is a "hide and run" game taking inspiration from [SOMA](https://en.wikipedia.org/wiki/Soma_(video_game)), [Penumbra](https://fr.wikipedia.org/wiki/Penumbra:_Overture) and [Amnesia](https://fr.wikipedia.org/wiki/Amnesia:_The_Dark_Descent).

### Mechanics
* [x] Run
* [x] Crouch
* [x] Crawl
* [ ] Hide

### Levels
* [x] First circle: Limbo
* [ ] Second circle: Lust
* [ ] Third circle: Gluttony
* [ ] Forth circle: Greed
* [ ] Fith circle: Wrath
* [ ] Sixth circle: Heresy
* [ ] Seventh circle: Violence
* [ ] Heighth circle: Fraud
* [ ] Ninth circle: Treachery

## Result at the end of the Wild Game Jam #9

So far, the game has a functional player (mostly from FPS tutorial), almost functional enemies (only "seeing" the player and not moving, yet) and only one level. As I am no artist, the texture are at best cheap, same for models.
Lot of the code can be improve, but I'll see that later. If you got any suggestion or comment, feel free to add an issue.

With love and pancakes from France, Mathieu NIVOLIEZ
